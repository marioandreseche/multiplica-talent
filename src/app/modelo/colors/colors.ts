export class Colors {
  nombre: string;
  year: Date;
  color: string;
  pantone_value: string;

  constructor(
    nombre: string,
    year: Date,
    color: string,
    pantone_value: string
  ) {
    this.nombre = nombre;
    this.year = year;
    this.color = color;
    this.pantone_value = pantone_value;
  }
}
