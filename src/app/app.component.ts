import { Component, OnInit } from "@angular/core";
import { ColorsService } from "./services/colors/colors.service";
import { Colors } from "./modelo/colors/colors";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "prueba";
  colors: any;
  mostrarColores = true;
  dataPaginacion: any;
  page: number = 1;

  constructor(private colorService: ColorsService) {}

  ngOnInit() {
    this.getColors(this.page);
  }
  getColors(pages) {
    this.colorService.getColors(pages).subscribe((colors: any) => {
      this.colors = colors.data;
      this.dataPaginacion = colors;
    });
  }
  obtenerColor(color) {
    this.mostrarColores = false;
    console.log(color);
    let colorCopiado = document.createElement("textarea");
    colorCopiado.value = color.color;
    document.body.appendChild(colorCopiado);
    colorCopiado.focus();
    colorCopiado.select();
    document.execCommand("copy");
    document.body.removeChild(colorCopiado);
  }
  siguiente(event) {
    this.getColors(event);
  }
  volver() {
    this, (this.mostrarColores = true);
  }
}
