import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Colors } from "src/app/modelo/colors/colors";

@Injectable({
  providedIn: "root",
})
export class ColorsService {
  constructor(private http: HttpClient) {}

  getColors(page: number): Observable<any> {
    return this.http.get<any>("https://reqres.in/api/colors?page=" + page);
  }
}
